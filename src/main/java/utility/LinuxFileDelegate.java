package utility;

public class LinuxFileDelegate implements FileDelegate {

    @Override
    public String getTokenLogFile() {
        return "resources/tokenLog.txt";
    }

    @Override
    public String getErrorLogFile() {
        return "resources/errorLog.txt";
    };

}
