package utility;

public interface FileDelegate {
    String getTokenLogFile();
    String getErrorLogFile();
}
