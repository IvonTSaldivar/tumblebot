package utility;

import java.sql.*;


public class SpellCommand {

    private StringBuilder spellText = new StringBuilder();
    private String spellResult;

    public String getResult() {
        return spellResult;
    }


    private void assembleText(ResultSet query) throws SQLException {
        source(query);
        spellText.append("__Casting__\n");
        casting(query);
        spellText.append("__Effect__\n");
        effect(query);
        spellText.append("__Description__");
        description(query);

        spellResult = spellText.toString();

    }

    //Spell name, source, school, levels
    private void source(ResultSet query) throws SQLException {
        spellText.append("__**").append(query.getString("name")).append("**__").append("\n");


        spellText.append("_").append(query.getString("source")).append("_").append("\n");


        spellText.append("**School** ").append(query.getString("school"));
        if(!query.getString("subschool").equals("")){
            spellText.append(" (").append(query.getString("subschool")).append(")");
        }
        if(!query.getString("descriptor").equals("")){
            spellText.append(" [").append(query.getString("descriptor")).append("]");
        }
        spellText.append("; ");


        spellText.append("**Level** ").append(query.getString("spell_level")).append("\n");

    }

    //casting time, components
    private void casting(ResultSet query) throws SQLException {
        spellText.append("**Casting Time** ").append(query.getString("casting_time")).append("\n");


        spellText.append("**Components** ").append(query.getString("components")).append("\n");

    }

    //range, area/target/etc, duration, saving throw, spell resistance
    private void effect(ResultSet query) throws SQLException {
        if(!query.getString("range").equals("")){
        spellText.append("**Range** ").append(query.getString("range")).append("\n");
        }


        if(!query.getString("area").equals("")){
            spellText.append("**Area** ").append(query.getString("area")).append("\n");
        } else if(!query.getString("effect").equals("")){
            spellText.append("**Effect** ").append(query.getString("effect")).append("\n");
        } else if(!query.getString("targets").equals("")) {
            spellText.append("**Targets** ").append(query.getString("targets")).append("\n");
        }


        spellText.append("**Duration** ").append(query.getString("duration")).append("\n");


        if(!query.getString("saving_throw").equals("")){
            spellText.append("**Saving Throw** ").append(query.getString("saving_throw")).append("; ");
        }
        if(!query.getString("spell_resistence").equals("")){
            spellText.append("**Spell Resistance** ").append(query.getString("spell_resistence")).append("\n");
        }

    }

    //description
    private void description(ResultSet query) throws SQLException {
        spellText.append(query.getString("description_formated")
                .replaceAll("\\*", "\\\\*")     //Replace * with \*
                .replaceAll("<p>", "\n")        //remove <p>
                .replaceAll("</p>", "\n")       //</p> is a line break
                .replaceAll("<[/]*i>", "*")     //<i> and </i> are italics
                .replaceAll("<table>","\n")
                .replaceAll("</table>", "\n")
                .replaceAll("<tr>", "")
                .replaceAll("</tr>", "\n")
                .replaceAll("<th.*?>", " __")
                .replaceAll("</th>", "__ |")
                .replaceAll("<td>", " ")
                .replaceAll("</td>", " |")
                .replaceAll("<[/]*b>", "**")
            );
    }



    public SpellCommand(String inputSpell){
        System.out.println(inputSpell);
            Connection connection = null;
            try {
                inputSpell = inputSpell.replaceAll("'", "''");
                long startTime = System.currentTimeMillis();

                //connection = DriverManager.getConnection("jdbc:sqlite::s3://elasticbeanstalk-us-east-2-946699923676/spells.sqlite");
                connection = DriverManager.getConnection("jdbc:sqlite:spells.sqlite");


                String query = "SELECT * FROM spell_full_trim_Sheet0 WHERE name_lowercase = '" + inputSpell + "'";
                String countQuery = "SELECT count(*) FROM spell_full_trim_Sheet0 WHERE name_lowercase = '" + inputSpell + "'";

                Statement statement = connection.createStatement();
                ResultSet count = statement.executeQuery(countQuery);


                if(count.next() && count.getInt(1) != 1){
                    spellText.append(count.getInt(1)).append(" records were found matching '").append(inputSpell).append("'. Perhaps you spelled it incorrectly?");
                    spellResult = spellText.toString();
                } else {
                    ResultSet results = statement.executeQuery(query);
                    while (results.next()) {
                        assembleText(results);
                    }
                }
                long endTime = System.currentTimeMillis();


            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    private void getClasses(ResultSet results){
        StringBuilder returnVal = new StringBuilder();

    }
}
