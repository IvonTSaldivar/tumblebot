package utility;

import java.sql.*;


public class FeatCommand {

    private StringBuilder featText = new StringBuilder();
    private String featResult;

    public String getResult() {
        return featResult;
    }


    private void assembleText(ResultSet query) throws SQLException {
        source(query);
        casting(query);

        featResult = featText.toString();

    }

    //Spell name, source, school, levels
    private void source(ResultSet query) throws SQLException {
        featText.append("__**").append(query.getString("name"));

        featText.append("  (").append(query.getString("type"));

        int teamwork = query.getInt("teamwork");
        int grit = query.getInt("critical");
        int style = query.getInt("style");
        int panache = query.getInt("panache");
        int betrayal = query.getInt("betrayal");
        int targeting = query.getInt("targeting");
        int esoteric = query.getInt("esoteric");
        int stare = query.getInt("stare");
        int weapon_mastery = query.getInt("weapon_mastery");
        int item_mastery = query.getInt("item_mastery");
        int armor_mastery = query.getInt("armor_mastery");
        int shield_mastery = query.getInt("shield_mastery");
        int blood_hex = query.getInt("blood_hex");
        int trick = query.getInt("trick");

        if(teamwork == 1) {
            featText.append(", ").append("Teamwork");
        }
        if(grit == 1) {
            featText.append(", ").append("Grit");
        }
        if(style == 1) {
            featText.append(", ").append("Style");
        }
        if(panache == 1) {
            featText.append(", ").append("Panache");
        }
        if(betrayal == 1) {
            featText.append(", ").append("Betrayal");
        }
        if(targeting == 1) {
            featText.append(", ").append("Targeting");
        }
        if(esoteric == 1) {
            featText.append(", ").append("Esoteric");
        }
        if(stare == 1) {
            featText.append(", ").append("Stare");
        }
        if(weapon_mastery == 1) {
            featText.append(", ").append("Weapon Mastery");
        }
        if(item_mastery == 1) {
            featText.append(", ").append("Item Mastery");
        }
        if(armor_mastery == 1) {
            featText.append(", ").append("Armor Mastery");
        }
        if(shield_mastery == 1) {
            featText.append(", ").append("Shield Mastery");
        }
        if(blood_hex == 1) {
            featText.append(", ").append("Blood Hex");
        }
        if(trick == 1) {
            featText.append(", ").append("Trick");
        }

        featText.append(")");

        featText.append("**__").append("\n");

        featText.append("_").append(query.getString("source")).append("_").append("\n");


        featText.append("_").append(query.getString("description")).append("_\n");


        featText.append("**Prerequisites:** ").append(query.getString("prerequisites")).append("\n");

    }

    //casting time, components
    private void casting(ResultSet query) throws SQLException {
        featText.append("**Benefit:** ").append(query.getString("benefit")).append("\n");

        if(!query.getString("normal").equals("")) {
            featText.append("**Normal:** ").append(query.getString("normal")).append("\n");
        }
        if(!query.getString("special").equals("")) {
            featText.append("**Special:** ").append(query.getString("special")).append("\n");
        }

        if(!query.getString("goal").equals("")) {
            featText.append("**Goal:** ").append(query.getString("goal")).append("\n");
        }
        if(!query.getString("completion_benefit").equals("")) {
            featText.append("**Completion:** ").append(query.getString("completion_benefit")).append("\n");
        }

        if(!query.getString("note").equals("")) {
            featText.append("**Note:** ").append(query.getString("note")).append("\n");
        }


    }

    //range, area/target/etc, duration, saving throw, spell resistance
    private void effect(ResultSet query) throws SQLException {
        if(!query.getString("range").equals("")){
        featText.append("**Range** ").append(query.getString("range")).append("\n");
        }


        if(!query.getString("area").equals("")){
            featText.append("**Area** ").append(query.getString("area")).append("\n");
        } else if(!query.getString("effect").equals("")){
            featText.append("**Effect** ").append(query.getString("effect")).append("\n");
        } else if(!query.getString("targets").equals("")) {
            featText.append("**Targets** ").append(query.getString("targets")).append("\n");
        }


        featText.append("**Duration** ").append(query.getString("duration")).append("\n");


        if(!query.getString("saving_throw").equals("")){
            featText.append("**Saving Throw** ").append(query.getString("saving_throw")).append("; ");
        }
        if(!query.getString("spell_resistence").equals("")){
            featText.append("**Spell Resistance** ").append(query.getString("spell_resistence")).append("\n");
        }

    }

    //description
    private void description(ResultSet query) throws SQLException {
        featText.append(query.getString("description_formated")
                .replaceAll("\\*", "\\\\*")     //Replace * with \*
                .replaceAll("<p>", "\n")        //remove <p>
                .replaceAll("</p>", "\n")       //</p> is a line break
                .replaceAll("<[/]*i>", "*")     //<i> and </i> are italics
                .replaceAll("<[/]*b>", "**")     //<i> and </i> are italics
                .replaceAll("<table>","\n")
                .replaceAll("</table>", "\n")
                .replaceAll("<tr>", "")
                .replaceAll("</tr>", "\n")
                .replaceAll("<th.*?>", " __")
                .replaceAll("</th>", "__ |")
                .replaceAll("<td>", " ")
                .replaceAll("</td>", " |")
                .replaceAll("<[/]*b>", "**")
            );
    }



    public FeatCommand(String inputFeat){
        System.out.println(inputFeat);
            Connection connection = null;
            try {
                inputFeat = inputFeat.replaceAll("'", "''");
                long startTime = System.currentTimeMillis();

                //connection = DriverManager.getConnection("jdbc:sqlite::s3://elasticbeanstalk-us-east-2-946699923676/spells.sqlite");
                connection = DriverManager.getConnection("jdbc:sqlite:featsdb.sqlite");


                String query = "SELECT * FROM feats WHERE name_lowercase = '" + inputFeat + "'";
                String countQuery = "SELECT count(*) FROM feats WHERE name_lowercase = '" + inputFeat + "'";

                Statement statement = connection.createStatement();
                ResultSet count = statement.executeQuery(countQuery);


                if(count.next() && count.getInt(1) != 1){
                    featText.append(count.getInt(1)).append(" records were found matching '").append(inputFeat).append("'. Perhaps you spelled it incorrectly?");
                    featResult = featText.toString();
                } else {
                    ResultSet results = statement.executeQuery(query);
                    while (results.next()) {
                        assembleText(results);
                    }
                }
                long endTime = System.currentTimeMillis();


            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    private void getClasses(ResultSet results){
        StringBuilder returnVal = new StringBuilder();

    }
}
