package utility;

public class Queue {

    private Node head;
    private Node foot;
    private int count;

    private class Node{
        String element;
        Node next;

        Node(String val){
            element = val;
        }
    }

    //Checks whether a string is actually a valid number.
    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }


    /**
     * Checks to see if the queue is empty.
     * @return Is the queue empty? Y/N
     */
    public boolean isEmpty(){
        return (count == 0);
    }

    public int getCount(){
        return count;
    }

    /**
     * Reads the value at the front of the queue, and resinerts it at the back.
     * @return The value at the front of the queue
     */
    String readString(){
        if (count != 0) {
            String returnVal = head.element;
            head = head.next;
            loop(returnVal);
            return returnVal;
        }
        return "Empty!";
    }

    /**
     * Ensures values removed from the queue don't get lost - merely reinserted at the back
     * @param reinsert String to be reinserted
     */
    private void loop(String reinsert){
        if(head == null){
            head = new Node(reinsert);
            foot = head;
        } else {
            Node temp = new Node(reinsert);
            foot.next = temp;
            foot = temp;
        }
    }

    /**
     * Insert a new string into the queue.
     * @param val String to be inserted
     */
    private void push(String val){
        ++count;
        if(head == null){
            head = new Node(val);
            foot = head;
        } else {
            Node temp = new Node(val);
            foot.next = temp;
            foot = temp;
        }
    }

    /**
     * Constructor. Formats each character/integer in args as it's own entry.
     * @param args
     */
    public Queue(String[] args){
        StringBuilder builder = new StringBuilder();
        for(String x: args){
            boolean pureInt = true;

            for(int i = 0; i < x.length(); ++i){
                if(!Character.isDigit(x.charAt(i))){
                    pureInt = false;
                }
            }
            if(pureInt) {
                builder.append(x);
                builder.append(" ");
            }
            else {
                for (int i = 0; i < x.length(); ++i){
                    if(Character.isDigit(x.charAt(i))){
                        builder.append(x.charAt(i));
                    }
                    else {
                        builder.append(" ");
                        builder.append(x.charAt(i));
                        builder.append(" ");
                    }
                }
            }
        }
        args = builder.toString().trim().split("\\s+");

//        for(String x : args){
//            push(x);
//        }
        for(int i = 0; i < args.length; ++i){
            if(args[i].equalsIgnoreCase("d")) {
                if (i != 0 && isInteger(args[i - 1])) {
                    push(args[i]);
                } else {
                    push("1");
                    push(args[i]);
                }
            } else {
                push(args[i]);
            }
                //log(args[i]);
        }
    }

    int logCount = 0;
    private void log(){
        System.out.println(logCount);
        logCount++;
    }

    private void log(String x){
        System.out.println(x);
    }
}
