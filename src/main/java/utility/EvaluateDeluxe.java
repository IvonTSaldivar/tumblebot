package utility;



/******************************************************************************
 *  Compilation:  javac EvaluateDeluxe.java
 *  Execution:    java EvaluateDeluxe
 *  Dependencies: Stack.java
 *
 *  Evaluates arithmetic expressions using Dijkstra's two-stack algorithm.
 *  Handles the following binary operators: +, -, *, / and parentheses.
 *
 *  % echo "3 + 5 * 6 - 7 * ( 8 + 5 )" | java EvaluateDeluxe
 *  -58.0
 *
 *
 *  Limitiations
 *  --------------
 *    -  can easily add additional operators and precedence orders, but they
 *       must be left associative (exponentiation is right associative)
 *    -  assumes whitespace between operators (including parentheses)
 *
 *  Remarks
 *  --------------
 *    -  can eliminate second phase if we enclose stdlib expression
 *       in parentheses (and, then, could also remove the test
 *       for whether the operator stack is empty in the inner while loop)
 *    -  see http://introcs.cs.princeton.edu/java/11precedence/ for
 *       operator precedence in Java
 *
 ******************************************************************************/


import java.util.ArrayList;
import java.util.Stack;
import java.util.TreeMap;

public class EvaluateDeluxe {

    public int result;
    public ArrayList<Dice> rolls = new ArrayList<>();

    // result of applying binary operator op to two operands val1 and val2
    public int eval(String op, int val1, int val2) {
        if (op.equals("+")) return val1 + val2;
        if (op.equals("-")) return val1 - val2;
        if (op.equals("/")) return val1 / val2;
        if (op.equals("*")) return val1 * val2;
        if (op.equals("d") || op.equals("D")) {
            Dice die = new Dice(val1, val2);
            rolls.add(die);
            return die.getTotal();
        }
        throw new RuntimeException("Invalid operator");
    }

    public EvaluateDeluxe(Queue queue){
        // precedence order of operators
        TreeMap<String, Integer> precedence = new TreeMap<String, Integer>();
        precedence.put("(", 0);   // for convenience with algorithm
        precedence.put(")", 0);
        precedence.put("+", 1);   // + and - have lower precedence than * and /
        precedence.put("-", 1);
        precedence.put("*", 2);
        precedence.put("/", 2);
        precedence.put("d", 3);
        precedence.put("D", 3);

        Stack<String> ops  = new Stack<String>();
        Stack<Integer> vals = new Stack<Integer>();

        for(int i = 0; i < queue.getCount(); ++i) {

            // read in next token (operator or value)
            String s = queue.readString();

            // token is a value
            if (!precedence.containsKey(s)) {
                vals.push(Integer.parseInt(s));
                continue;
            }

            // token is an operator
            while (true) {

                // the last condition ensures that the operator with higher precedence is evaluated first
                if (ops.isEmpty() || s.equals("(") || (precedence.get(s) > precedence.get(ops.peek()))) {
                    ops.push(s);
                    break;
                }

                // evaluate expression
                String op = ops.pop();

                // but ignore left parentheses
                if (op.equals("(")) {
                    assert s.equals(")");
                    break;
                }

                // evaluate operator and two operands and push result onto value stack
                else {
                    int val2 = vals.pop();
                    int val1 = vals.pop();
                    vals.push(eval(op, val1, val2));
                }
            }
        }

        // finished parsing string - evaluate operator and operands remaining on two stacks
        while (!ops.isEmpty()) {
            String op = ops.pop();
            int val2 = vals.pop();
            int val1 = vals.pop();

            vals.push(eval(op, val1, val2));
        }

        // last value on stack is value of expression
        result = vals.pop();
        assert vals.isEmpty();
        assert ops.isEmpty();
    }
}