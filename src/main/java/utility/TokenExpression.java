package utility;

import mainBot.BotUtils;

public class TokenExpression {
    private Token target;

    private boolean isTokenValid = false;

    private StringBuilder stringResult = new StringBuilder();

    private DiceExpression tokenDice;


    public boolean isTokenValid(){
        return isTokenValid;
    }

    TokenExpression(){
        isTokenValid = false;
    }

    TokenExpression(String message, int diceResult){
        String[] splitAtToken = message.split(" ", 2);
        if (BotUtils.containsKey(splitAtToken[0])){
            isTokenValid = true;
            target = BotUtils.getToken(splitAtToken[0]);
            if (diceResult <= target.getDifficultyClass()){     //Defender wins ties
                stringResult.append(" Miss!");
            } else {
                stringResult.append(" Hit! \n");
                if (splitAtToken.length > 1) {
                    tokenDice = new DiceExpression(splitAtToken[1]);
                    if (tokenDice.getStringExpression() != null) {
                        stringResult.append("`");
                        stringResult.append(tokenDice.getStringExpression());
                        stringResult.append("`");
                    }

                    if (tokenDice.getStringExpression() != null) {
                        stringResult.append(" = ");
                        stringResult.append(tokenDice.getResultExpression());
                        stringResult.append(" = `");
                        stringResult.append(tokenDice.getResult());
                        stringResult.append("`");
                    }
                    stringResult.append(" on ");
                    stringResult.append(target.getKey());
                }
            }
        }
        //else do filler stuff to avoid breaking
        stringResult.append("");
    }

    public String toString(){
        return stringResult.toString();
    }
}
