package utility;

public class HelpDialog {

    private StringBuilder diceHelp = new StringBuilder(400);
    private StringBuilder tokenHelp = new StringBuilder(1700);
    private StringBuilder printHelp = new StringBuilder(450);
    private StringBuilder miscHelp = new StringBuilder();



    public HelpDialog(String prefix){
        //Into help
        diceHelp.append("To run a command, type a '");
        diceHelp.append(prefix);
        diceHelp.append("' followed by whichever command you wish to run.\n");



        //Dice help
        diceHelp.append("\n__**");
        diceHelp.append(prefix);
        diceHelp.append("dice**__\n\t");
        diceHelp.append(prefix);
        diceHelp.append("dice <dice expression>\n");
        diceHelp.append("\t\tEx: `");
        diceHelp.append(prefix);
        diceHelp.append("dice 1d20 + 3`\n");
        diceHelp.append("\t\tEx: `");
        diceHelp.append(prefix);
        diceHelp.append("dice (((1d20 + 3)*5)/ 7) -11` \n");

        diceHelp.append("\t");
        diceHelp.append(prefix);
        diceHelp.append("dice <dice expression> <$token> <conditional dice>\n");
        diceHelp.append("\t\tEx: `");
        diceHelp.append(prefix);
        diceHelp.append("dice 1d20 $goblin 1d6 + 2`\n");

        diceHelp.append("\t");
        diceHelp.append(prefix);
        diceHelp.append("dice <dice expression> #Everything after the '#' is ignored\n");
        diceHelp.append("\t\t Ex: `");
        diceHelp.append(prefix);
        diceHelp.append("dice 1d20 + 3 #Power Attack!`\n");

//        diceHelp.append("Length: ");
//        diceHelp.append(diceHelp.toString().length());


        //Token help
        tokenHelp.append("\n__**A brief guide to tokens**__\n");
        tokenHelp.append("Tokens are designed to eliminate some of the tedious back-and-forth required between DM and PC when an attack is made. ");
        tokenHelp.append("Instead of the player rolling, followed by the GM confirming hit or miss, followed by the player rolling damage, the DM ");
        tokenHelp.append("can simply tell TumbleBot what a creature's AC is by using a 'token', which will allow the player to roll dice conditionally ");
        tokenHelp.append("when targeting that token. \n");

        tokenHelp.append("\n**To create a token,** enter\n\t`");
        tokenHelp.append(prefix);
        tokenHelp.append("token <token name> <token DC> <optional description>`\n");
        tokenHelp.append("\t\twhere <token name> is what the PC will use to target the token,\n");
        tokenHelp.append("\t\t <token DC> is the target Armor (or Difficulty) Class of the token,\n");
        tokenHelp.append("\t\t and <optional description> is an optional description to identify the token to the DM. The players will not be able to " +
                "see this in the course of normal play.\n");
        tokenHelp.append("\t\t\tEx: `");
        tokenHelp.append(prefix);
        tokenHelp.append("token goblinDog 12`\n");
        tokenHelp.append("\t\t\t\tCreates a token called 'goblinDog' with AC 12.\n");
        tokenHelp.append("\t\t\tEx: `");
        tokenHelp.append(prefix);
        tokenHelp.append("token goblinBoss 15 The goblin boss! He's so scary!`\n");
        tokenHelp.append("\t\t\t\tCreates a token called 'goblinBoss' with AC 15 and a brief description.\n");

        tokenHelp.append("**To delete a token,** enter\n\t");
        tokenHelp.append("`");
        tokenHelp.append(prefix);
        tokenHelp.append("token <token name>`\n");
        tokenHelp.append("\t\tEx: `");
        tokenHelp.append(prefix);
        tokenHelp.append("token goblinDog`\n");
        tokenHelp.append("\t\t\tDeletes the token associated with goblinDog.\n");

        tokenHelp.append("**To use a token in play,** enter your dice expression followed by $<token name> <damage dice>\n");
        tokenHelp.append("\tEx: `");
        tokenHelp.append(prefix);
        tokenHelp.append("dice 1d20 + 3 $goblinDog 2d6 + 5 #Greatsword swing!`\n");
        tokenHelp.append("\t\tIf the results of (1d20 + 3) are greater than or equal to the AC of the goblinDog, the " +
                "bot will roll 2d6 + 5. If the results are less than the AC of the goblinDog, no extra dice will be rolled.\n");

        tokenHelp.append("\nTokens are not case-sensitive. goblindog, GOBLINDOG, goblinDog, and gObLiNdOg will all match to the same token.\n");

//        tokenHelp.append("Length: ");
//        tokenHelp.append(tokenHelp.toString().length());

        //Print help

        printHelp.append("\n__**");
        printHelp.append(prefix);
        printHelp.append("Print**__\n\t`");

        printHelp.append(prefix);
        printHelp.append("print all`\n");
        printHelp.append("\t\tPrints a list of all existing tokens\n\t");
        printHelp.append(prefix);
        printHelp.append("print <token name>\n");
        printHelp.append("\t\tPrints information for that token in the form \"$<token name> (AC/DC) [optional description]\"");

        printHelp.append("\t\t\tEx: `");
        printHelp.append(prefix);
        printHelp.append("print goblinDog` outputs\n");
        printHelp.append("\t\t\t\t`$goblinDog (12)`\n");

        printHelp.append("\t\t\tEx: `");
        printHelp.append(prefix);
        printHelp.append("print goblinBoss` outputs\n");
        printHelp.append("\t\t\t\t`goblinBoss (15) [The goblin boss! He's so scary!]`\n");


        //Other help

        miscHelp.append("__**Misc.**__\n\t`");
        miscHelp.append(prefix);
        miscHelp.append("test` - A quick command you can use to check whether TumbleBot is online\n\t`");
        miscHelp.append(prefix);
        miscHelp.append("help` - A simple help page.");


//        printHelp.append("Length: ");
//        printHelp.append(printHelp.toString().length());

    }

    public String getDiceHelp(){
        return diceHelp.toString();
    }

    public String getTokenHelp(){
        return tokenHelp.toString();
    }

    public String getPrintHelp(){
        return printHelp.toString();
    }

    public String getMiscHelp() {
        return miscHelp.toString();
    }
}
