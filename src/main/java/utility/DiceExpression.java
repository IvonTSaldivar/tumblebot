package utility;

import java.util.ArrayList;

public class DiceExpression {
    private StringBuilder stringExpression;
    private StringBuilder expressionWithValues;
    private EvaluateDeluxe calculator;

    int count = 0;
    private void log(){
        System.out.println("  " + count);
        count++;
    }


    public DiceExpression(String input) {
        try {
            if (input == null) {
                throw new NumberFormatException();
            }
            String[] splitAtWhitespace = input.split(" ");
            Queue expression = new Queue(splitAtWhitespace);
            stringExpression = new StringBuilder();
            expressionWithValues = new StringBuilder();

            for (int i = 0; i < expression.getCount(); ++i) {
                stringExpression.append(expression.readString());
            }
            calculator = new EvaluateDeluxe(expression);
            ArrayList<Dice> rolls = calculator.rolls;


            StringBuilder prevVal = new StringBuilder("");
            StringBuilder currVal = new StringBuilder("");
            StringBuilder nextVal = new StringBuilder(expression.readString());


            for (int i = 0; i < expression.getCount(); ++i) {
                if (currVal.toString().equals("d")) {
                    expressionWithValues.append("[");
                    Dice roll = rolls.remove(0);
                    if (roll.getQuantity() != 1) {
                        for (int x : roll.getRolls()) {
                            expressionWithValues.append(x);
                            expressionWithValues.append("+");
                        }
                        expressionWithValues.deleteCharAt(expressionWithValues.lastIndexOf("+"));
                    } else expressionWithValues.append(roll.getTotal());
                    expressionWithValues.append("]");
                } else if (!prevVal.toString().equals("d") && !nextVal.toString().equals("d")) {
                    expressionWithValues.append(currVal.toString());
                }

                prevVal.replace(0, prevVal.length(), currVal.toString());
                currVal.replace(0, currVal.length(), nextVal.toString());
                nextVal.replace(0, nextVal.length(), expression.readString());
            }


            if ((expressionWithValues.toString().length() > 0) && !(expressionWithValues.toString().charAt(expressionWithValues.toString().length() - 1) == ']')) {
                expressionWithValues.append(currVal.toString());
            }
        } catch(NumberFormatException nfe){
            nfe.printStackTrace();
            stringExpression = null;
            expressionWithValues = null;
            calculator = null;
            return;
        }
    }

    public String getResultExpression(){ return expressionWithValues.toString();
    }


    public int getResult(){
        return calculator.result;
    }

    public String getStringExpression(){
        return stringExpression.toString();
    }

}


