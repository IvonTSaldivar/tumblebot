package utility;

import java.util.ArrayList;
import java.util.Random;



public class Dice {
    private int sides;
    private int quantity;
    private int total = 0;

    private ArrayList<Integer> rolls = new ArrayList<>();

    public int getSides(){
        return sides;
    }
    public int getQuantity(){
        return quantity;
    }

    public int getTotal(){
        return total;
    }

    public ArrayList<Integer> getRolls() {
        return rolls;
    }

    public String toString(){
        return (quantity + "d" + sides);
    }

    Dice(int q, int s){
        quantity = q;
        sides = s;

        for(int i = 0; i < quantity; ++i){
            int roll = new Random().nextInt(sides) + 1;
            rolls.add(roll);
            total += roll;
        }
    }
}
