package utility;

public class Token {
    private String key;
    public String lowerCaseKey;
    private int difficultyClass;
    private String name;




    public String getKey(){
        return key;
    }

    public String getLowerCaseKey() {return lowerCaseKey; }


    public int getDifficultyClass(){
        return difficultyClass;
    }

    public String getName(){
        return name;
    }


    public String toString(){
        return "$" + key + " (" + difficultyClass + ") " + "[" + name + "]";
    }


    public Token(String label, int dc){
        key = label;
        lowerCaseKey = key.toLowerCase();
        difficultyClass = dc;
        name = "no name given";
    }

    public Token(String label, int dc, String description){
        key = label;
        lowerCaseKey = key.toLowerCase();
        difficultyClass = dc;
        name = description;
    }
}
