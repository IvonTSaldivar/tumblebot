package utility;


import mainBot.BotUtils;

public class DiceCommand {
    private StringBuilder returnVal;

    private DiceExpression diceResult;
    private TokenExpression tokenExpression;

    public DiceCommand(String message) {
        try {
            //Splitting message
            String[] splitAtComment = message.split("#", 2);   //!dice <expresson> $<token>, #Comment
            String[] splitAtToken = splitAtComment[0].split("\\$", 2);   //!dice <expression>, $<token>
            //String[] expression = splitAtToken[0].split(".?dice", 2);   //<expression>


            //Generating initial dice rolls
            if (splitAtToken[0].matches(".*[A-Za-z&&[^dD]].*") || splitAtToken[0].trim().length() == 0) { //If it contains an invalid character, specifically dD*/+-[]()
                returnVal = new StringBuilder();
                returnVal.append("error");
                return;
            }


            diceResult = new DiceExpression(splitAtToken[0].trim());
            //
            returnVal = new StringBuilder();
            if (diceResult.getStringExpression() != null) {
                returnVal.append("`");
                returnVal.append(diceResult.getStringExpression());
                returnVal.append("`");
            }
            if (splitAtToken.length != 1) {
                tokenExpression = confrontTheToken(splitAtToken[1].trim());
                returnVal.append(",  $");
                returnVal.append(splitAtToken[1].trim());
            } else tokenExpression = new TokenExpression();
            if (splitAtComment.length != 1) {
                returnVal.append("  #");
                returnVal.append(splitAtComment[1]);
            }
            if (diceResult.getStringExpression() != null) {
                if (diceResult.getResultExpression().length() > 0) {
                    returnVal.append(" = ");
                    returnVal.append(diceResult.getResultExpression().toLowerCase());
                }
                returnVal.append(" = `");
                returnVal.append(diceResult.getResult());
                returnVal.append("`");
            }
            if (tokenExpression.isTokenValid()) {
                returnVal.append(tokenExpression.toString());
                //do stuff
            }
        } catch(Exception e){
            e.printStackTrace();
            returnVal = new StringBuilder();
            returnVal.append("error");
            return;
        }
    }

    int count = 0;

    private void log(){
        System.out.println(count);
        count++;
    }

    //
    //TOKEN STUFF
    //

    private TokenExpression confrontTheToken(String expression){
        // $token (dice)
        return new TokenExpression(expression, diceResult.getResult());
    }



    //Return the result
    public String returnExpression() {
        return returnVal.toString();
    }
}
