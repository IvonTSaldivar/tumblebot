package utility;

public class WindowsFileDelegate implements FileDelegate {

    @Override
    public String getTokenLogFile() {
        return "resources/tokenLog.txt";
    }

    @Override
    public String getErrorLogFile() {
        return "resources/errorLog.txt";
    };

}
