package mainBot;

import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import utility.*;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static mainBot.BotUtils.BOT_PREFIX;

/**
 * TODO
 *
 */


public class MyEvents {

    /**
     * Stores each dice expression requested by
     */
    private ArrayList<String> diceMessage = new ArrayList<>();
    private ArrayList<String> spellMessage = new ArrayList<>();
    private ArrayList<String> featMessage = new ArrayList<>();
    private boolean isRepeat = false;


    private boolean getDiceStatement(String message){
        String lowerCaseMessage = message.toLowerCase();
        if (lowerCaseMessage.startsWith(BOT_PREFIX + "dice")){
            diceMessage.add(message.substring(BOT_PREFIX.length()+4, message.length()).trim());
            return true;
        } else if (lowerCaseMessage.contains("[dice")){
            Pattern pattern = Pattern.compile("\\[dice([^\\[]+)\\]", Pattern.CASE_INSENSITIVE);    //says redundant '/' - IT IS WRONG
            Matcher matcher = pattern.matcher(message);

            boolean foundOne = false;
            for (int i = 0; matcher.find(); ++i) {
                diceMessage.add(matcher.group(1).trim());
                foundOne = true;
            }
            return foundOne;
        }

        return false;
    }

    private boolean getSpellStatement(String message){
        String lowerCaseMessage = message.toLowerCase();
        if (lowerCaseMessage.startsWith(BOT_PREFIX + "spell ")){
            spellMessage.add(message.substring(BOT_PREFIX.length()+5, message.length()).trim());
            return true;
        } else if (lowerCaseMessage.contains("[spell")){
            Pattern pattern = Pattern.compile("\\[spell\\s+([^\\[]+)\\]", Pattern.CASE_INSENSITIVE);    //says redundant '/' - IT IS WRONG
            Matcher matcher = pattern.matcher(message);

            boolean foundOne = false;
            for (int i = 0; matcher.find(); ++i) {
                spellMessage.add(matcher.group(1).trim());
                foundOne = true;
            }
            return foundOne;
        }

        return false;
    }

    private boolean getFeatStatement(String message){
        String lowerCaseMessage = message.toLowerCase();
        if (lowerCaseMessage.startsWith(BOT_PREFIX + "feat ")){
            featMessage.add(message.substring(BOT_PREFIX.length()+4, message.length()).trim());
            return true;
        } else if (lowerCaseMessage.contains("[feat")){
            Pattern pattern = Pattern.compile("\\[feat\\s+([^\\[]+)\\]", Pattern.CASE_INSENSITIVE);    //says redundant '/' - IT IS WRONG
            Matcher matcher = pattern.matcher(message);

            boolean foundOne = false;
            for (int i = 0; matcher.find(); ++i) {
                featMessage.add(matcher.group(1).trim());
                foundOne = true;
            }
            return foundOne;
        }

        return false;
    }

    private boolean isAimedAtBot(String message){
        if (getDiceStatement(message) | getSpellStatement(message)| getFeatStatement(message)){
            return true;
        }
        if (message.startsWith(BOT_PREFIX)){
            return true;
        }

        return false;
    }



    /**
     * Of note:     \[dice(.+)\]
     * @param event
     */
    @EventSubscriber
    public void onMessageReceived(MessageReceivedEvent event) {
        //213113867927027712L


        if (!event.getAuthor().getName().equals("TumbleBot") && isAimedAtBot(event.getMessage().getContent())) {
            try {
                if (event.getMessage().getContent().split("\\w+").length > 1) { //IF CONSISTS OF MULTIPLE WORDS
                    //!dice
                    if (diceMessage.size() > 0) {
                        StringBuilder response = new StringBuilder();
                        for (int i = 0; i < diceMessage.size(); ++i) {
                            DiceCommand returnVal = new DiceCommand(diceMessage.get(i));
                            if (returnVal.returnExpression().equals("error")) {
                                response.append("\n").append(event.getAuthor().mention()).append(": `").append(diceMessage.get(i)).append("` is not a valid command - maybe you forgot a '#'?:");
                            } else {
                                response.append("\n").append(event.getAuthor().mention()).append(": ").append(returnVal.returnExpression());
                            }
                        }
                        BotUtils.sendMessage(event.getChannel(), response.toString());
                        diceMessage = new ArrayList<>();    //Delete the results so they don't get printed next time someone rolls dice
                    }

                    //!spell
                    if(spellMessage.size() > 0){
                        IChannel chan;
                        boolean isFlower = false;
                        if (!event.getChannel().isPrivate() && event.getGuild().getLongID() == 559694585148997664L) {
                            chan = getiChannel(event);
                        } else {
                            chan = event.getAuthor().getOrCreatePMChannel();
                            if(!event.getChannel().isPrivate()) {
                                BotUtils.sendMessage(event.getChannel(), "I have DM'd you the details of the spell, " + event.getAuthor().mention() + "!");
                            }
                        }
                        for (String aSpellMessage : spellMessage) {
                            Thread.sleep(100);
                            SpellCommand command = new SpellCommand(aSpellMessage.toLowerCase());
                            String[] brokenResult = command.getResult().split("\n");

                            StringBuilder finalResult = new StringBuilder();
                            finalResult = getStringBuilder(chan, brokenResult, finalResult);
                            BotUtils.sendMessage(chan, finalResult.toString());
                        }
                        spellMessage = new ArrayList<>();
                    }

                    //!feat
                    if(featMessage.size() > 0){
                        IChannel chan;
                        boolean isFlower = false;
                        if (!event.getChannel().isPrivate() && event.getGuild().getLongID() == 559694585148997664L) {
                            chan = getiChannel(event);
                        } else {
                            chan = event.getAuthor().getOrCreatePMChannel();
                            if(!event.getChannel().isPrivate()) {
                                BotUtils.sendMessage(event.getChannel(), "I have DM'd you the details of the feat, " + event.getAuthor().mention() + "!");
                            }
                        }
                        for (String aFeatMessage : featMessage) {
                            Thread.sleep(100);
                            FeatCommand command = new FeatCommand(aFeatMessage.toLowerCase());
                            String[] brokenResult = command.getResult().split("\n");

                            StringBuilder finalResult = new StringBuilder();
                            finalResult = getStringBuilder(chan, brokenResult, finalResult);
                            BotUtils.sendMessage(chan, finalResult.toString());
                        }
                        featMessage = new ArrayList<>();
                    }
                    //if admin
                    if(event.getAuthor().equals(BotUtils.getNewXToa())
                            || (!event.getChannel().isPrivate() && event.getAuthor().getPermissionsForGuild(event.getGuild()).contains(Permissions.ADMINISTRATOR))) {
                        if (event.getMessage().getContent().startsWith(BOT_PREFIX + "token")) {
                            String[] message = event.getMessage().getContent().split(" ", 2);
                            String returnMessage = BotUtils.addToken(message[1]);
                            if (returnMessage != null) {
                                try {
                                    BotUtils.sendMessage(event.getChannel(), event.getAuthor().mention() + ": " + returnMessage);
                                } catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            return;
                        }
                        if (event.getMessage().getContent().startsWith(BOT_PREFIX + "print")) {
                            String tokenKey = event.getMessage().getContent().split(BOT_PREFIX + "print", 2)[1].trim();
                            if (tokenKey.equals("all"))
                                printAll(event.getChannel());
                            else if (BotUtils.containsKey(tokenKey)) {
                                Token printedToken = BotUtils.getToken(tokenKey);
                                BotUtils.sendMessage(event.getChannel(), event.getAuthor().mention() + ": " + printedToken.toString());
                            }
                            return;
                        }
                    }
                } //END MULTIWORD COMMANDS


                //!test
                if (event.getMessage().getContent().startsWith(BOT_PREFIX + "test")) {
                    BotUtils.sendMessage(event.getChannel(), "If you're having problems with me, please let NewXToa know!");
                }
                //!help
                if (event.getMessage().getContent().startsWith(BOT_PREFIX + "help")) {
                    HelpDialog helpRequest = new HelpDialog(BOT_PREFIX);


                    BotUtils.sendMessage(event.getAuthor().getOrCreatePMChannel(), helpRequest.getDiceHelp());
                    //If admin, include token and print help
                    if(event.getAuthor().getPermissionsForGuild(event.getGuild()).contains(Permissions.ADMINISTRATOR)) {
                        BotUtils.sendMessage(event.getAuthor().getOrCreatePMChannel(), helpRequest.getTokenHelp());
                        BotUtils.sendMessage(event.getAuthor().getOrCreatePMChannel(), helpRequest.getPrintHelp());
                    }
                    BotUtils.sendMessage(event.getAuthor().getOrCreatePMChannel(), helpRequest.getMiscHelp());
                    BotUtils.sendMessage(event.getChannel(), "I sent you a DM!");
                }


                //Log off

                if (BotUtils.getNewXToa() != null && event.getMessage().getAuthor().equals(BotUtils.getNewXToa())) {
                    if (event.getMessage().getContent().startsWith(BOT_PREFIX + "quit")) {
                        BotUtils.sendMessage(event.getChannel(), "Fine! If you don't want me here, I'll just leave!");
                        BotUtils.onBotClose();
                        MainRunner.logout();
                    }

                    if(event.getMessage().getContent().startsWith(BOT_PREFIX + "restart")){
                        BotUtils.onBotClose();
                        MainRunner.restart();
                    }
                }

            }catch (Exception e) {
                StringBuilder eStackTrace = new StringBuilder();
                eStackTrace.append(e.toString());
                for (StackTraceElement content : e.getStackTrace()){
                    eStackTrace.append("\n");
                    eStackTrace.append("\t");
                    eStackTrace.append(content.toString());
                }
                //BotUtils.sendMessage(event.getChannel(), eStackTrace.toString());
                e.printStackTrace();
                System.out.println(e.getStackTrace());

                BotUtils.onExceptionThrown(eStackTrace.toString(), event.getMessage().getFormattedContent());
//                BotUtils.sendMessage(NewXToa.getOrCreatePMChannel(), "Something happened");
                BotUtils.sendMessage(event.getChannel(), "Something went wrong!\nI've sent NewXToa a DM, " +
                        "but I assure you he has no idea what's going on. Try not to use TumbleBot for the next half hour " +
                        "or so, and have an admin remove this message.");
                BotUtils.sendMessage(BotUtils.getNewXToa().getOrCreatePMChannel(), event.getAuthor().mention() + " entered \n"
                        + "```" + event.getMessage() + "```" + "and recieved the following error: \n"
                        + "```" + eStackTrace.toString() + "```");
                diceMessage = new ArrayList<>();
                spellMessage = new ArrayList<>();
                featMessage = new ArrayList<>();
            }
        }
    }

    private IChannel getiChannel(MessageReceivedEvent event) {
        IChannel chan;
        boolean isFlower;
        if (event.getChannel().getName().equals("gm-dice-stuff")) {
            chan = event.getChannel();
        } else{
            if (event.getChannel().getName().equals("bot-test")){   //This branch does nothing, see if above
                chan = event.getGuild().getChannelsByName("bot-test-2-electric-bugaloo").get(0);
            } else if (event.getChannel().getName().equals("bot-test-2-electric-bugaloo")){
                chan = event.getGuild().getChannelsByName("bot-test").get(0);
            }else {
                chan = event.getGuild().getChannelsByName("bot-test-room").get(0);
            }
            BotUtils.sendMessage(chan, "\n---------------------\n");
            if (!event.getChannel().getName().equals("bot-test-room")) {
                BotUtils.sendMessage(event.getChannel(), "<#" + chan.getStringID() + ">");
            }
            isFlower = true;
        }
        return chan;
    }

    private StringBuilder getStringBuilder(IChannel chan, String[] brokenResult, StringBuilder finalResult) throws InterruptedException {
        for (String aBrokenResult : brokenResult) {
            if (finalResult.toString().length() + aBrokenResult.length() + 1 > 2000) {
                BotUtils.sendMessage(chan, finalResult.toString());
                Thread.sleep(100);
                finalResult = new StringBuilder();
            }
            finalResult.append(aBrokenResult).append("\n");
        }
        return finalResult;
    }



    /*
    //Sends a message to a user when they come online (or more accurately, stop being offline)
    @EventSubscriber
    public void userChangedStates(PresenceUpdateEvent event){
        if (event.getOldPresence().getStatus() == StatusType.OFFLINE){
            BotUtils.sendMessage(event.getUser().getOrCreatePMChannel(), "Poke!\nI'm going to poke you every time your status changes from \"Offline\", and it'll be really annoying!");
        }
    }
    */

    private void printAll(IChannel source){
        Set<String> allKeys = BotUtils.tokenNames.keySet();
        StringBuilder returnList = new StringBuilder();
        for (String key : allKeys) {
            returnList.append(BotUtils.tokenNames.get(key).toString());
            returnList.append("\n");
        }
        BotUtils.sendMessage(source, returnList.toString());
    }
    private void checkForAndPrintToken(IChannel source, String message){

    }
}