package mainBot;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IUser;

public class MainRunner {

    public static IDiscordClient cli;

    public static String tumbleToken =  "NDQ0NjY2NTcyOTc2NDIyOTMz.DdfPmg.KfeUWEzjcnselNTdSj74DL6OLS8";

    public static int currToken = 0;

    public static String testToken = "NDMwNzUyODY1NTc3ODYxMTIw.D30Y0A.BI8Ah5GfjjyWh2YfANPKsEa-QU0";

    public static void main(String[] args){
        if (args.length != 0){
            currToken = Integer.parseInt(args[0]);
        }
        initialize(currToken);

        BotUtils.onBotStart();

        // TumbleBot
        // testBot

    }

    public static void initialize(int whichToken) {
        String botToken = (whichToken == 0) ? tumbleToken : testToken;
        String prefix = (whichToken == 0) ? "!" : "/";

        cli = BotUtils.getBuiltDiscordClient(botToken, prefix);

        // Register a listener via the EventSubscriber annotation which allows for organisation and delegation of events
        cli.getDispatcher().registerListener(new MyEvents());

        // Only login after all events are registered otherwise some may be missed.
        cli.login();
    }

    public static void logout(){
        cli.logout();
    }
    public static void restart(){
        BotUtils.onBotClose();
        logout();
        initialize(currToken);
    }


}