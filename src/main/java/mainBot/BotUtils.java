package mainBot;

import javafx.util.converter.NumberStringConverter;
import sx.blah.discord.handle.obj.IUser;
import utility.FileDelegate;
import utility.LinuxFileDelegate;
import utility.Token;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;
import utility.WindowsFileDelegate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;

public class BotUtils {
    // Constants for use throughout the bot
    public static String BOT_PREFIX;

    //For tokens
    static public Hashtable<String, Token> tokenNames = new Hashtable<>();

    //To determine where to
    static private FileDelegate fileNames;
    static private LinuxFileDelegate linuxFiles = new LinuxFileDelegate();
    static private WindowsFileDelegate windowsFiles = new WindowsFileDelegate();




    //Get all the pre-existing tokens from the file
    public static void onBotStart(){
        if (System.getProperty("os.name").toLowerCase().startsWith("windows"))
            fileNames = windowsFiles;
        else fileNames = linuxFiles;
        Scanner fileInput = null;

        try {
            fileInput = new Scanner(new FileReader(fileNames.getTokenLogFile()));

            while (fileInput.hasNextLine()){
                String tokenData;
                if(!(tokenData = fileInput.nextLine()).isEmpty()) {
                    String[] tokenInfo = tokenData.split(" ");
                    StringBuilder tokenName = new StringBuilder();
                    for (int i = 2; i < tokenInfo.length; ++i){
                        tokenName.append(tokenInfo[i]);
                        if (i < tokenInfo.length - 1) tokenName.append(" ");
                    }
                    tokenNames.put(tokenInfo[0].toLowerCase(), new Token(tokenInfo[0], Integer.parseInt(tokenInfo[1]), tokenName.toString()   ));
                }
            }
        }
        catch (FileNotFoundException | NullPointerException e){
            e.printStackTrace();
        }

        PrintWriter errorLog = null;
        try {

            errorLog = new PrintWriter(new FileOutputStream(fileNames.getErrorLogFile(), false));
            errorLog.print("");
            errorLog.close();

        } catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }


    //When the file is closed, put all the tokens in the hashmap into a file
    public static void onBotClose(){
        Set<String> listOfTokens = tokenNames.keySet();
       if (listOfTokens.size() != 0){
           PrintWriter fileOutput = null;
           try {
               fileOutput = new PrintWriter(new FileOutputStream(fileNames.getTokenLogFile(), false ));
               for(String x : listOfTokens) {
                   fileOutput.print(tokenNames.get(x).getKey() + " " + tokenNames.get(x).getDifficultyClass() + " " + tokenNames.get(x).getName() + "\n");
               }
               fileOutput.close();
           }
           catch (FileNotFoundException e) {
               e.printStackTrace();
           }
       }
    }

    public static void onExceptionThrown(String exception, String source){
        PrintWriter errorLog = null;
        try {

            errorLog = new PrintWriter(new FileOutputStream(fileNames.getErrorLogFile(), true));
            errorLog.print(source);
            errorLog.print("\n");
            errorLog.print(exception);
            errorLog.print("\n\n");
            errorLog.close();

        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    //I need to figure out how to sync the hash table with the file when I open and close the program, rather than doing it on the fly
    static public String addToken(String message) {
        Token newToken;
        if (message != null) {
            String[] splitAtKey = message.split("[ ]+", 3);  //Splits it into key, dc, name...
            if (!splitAtKey[0].trim().equals("all")) {
                //Modifying a token
                if (splitAtKey.length == 3) {
                    newToken = new Token(splitAtKey[0].trim(), Integer.parseInt(splitAtKey[1]), splitAtKey[2]);
                    tokenNames.put(newToken.getLowerCaseKey(), newToken);
                    return "'" + newToken.toString() + "' has been created.";
                    //still modifying a token
                } else if (splitAtKey.length == 2) {
                    newToken = new Token(splitAtKey[0].trim(), Integer.parseInt(splitAtKey[1]));
                    tokenNames.put(newToken.getLowerCaseKey(), newToken);
                    return "'" + newToken.toString() + "' has been created.";
                } else {
                    tokenNames.remove(splitAtKey[0].trim().toLowerCase());
                    return splitAtKey[0] + " has been deleted from the legend.";
                }
            } else {
                return "'all' may not be modified";
            }
        }
        return "";
    }

    static public boolean containsKey(String key){
        return tokenNames.containsKey(key.toLowerCase());
    }

    static public Token getToken(String key){
        return tokenNames.get(key.toLowerCase());
    }

    // Handles the creation and getting of a IDiscordClient object for a token
    static IDiscordClient getBuiltDiscordClient(String botToken, String prefix){
        BOT_PREFIX = prefix;
        // The ClientBuilder object is where you will attach your params for configuring the instance of your bot.
        // Such as withToken, setDaemon etc
        return new ClientBuilder()
                .withToken(botToken)
                .build();

    }

    // Helper functions to make certain aspects of the bot easier to use.
    static void sendMessage(IChannel channel, String message){
        // This might look weird but it'll be explained in another page.
        RequestBuffer.request(() -> {
            try{
                channel.sendMessage(message);
            } catch (DiscordException e){
                System.err.println("Message could not be sent with error: ");
                e.printStackTrace();
            }
        });

        /*
        // The below example is written to demonstrate sending a message if you want to catch the RLE for logging purposes
        RequestBuffer.request(() -> {
            try{
                channel.sendMessage(message);
            } catch (RateLimitException e){
                System.out.println("Do some logging");
                throw e;
            }
        });
        */
    }

    public static IUser getNewXToa(){
        return MainRunner.cli.fetchUser(213113867927027712L);
    }
}
